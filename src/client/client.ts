import {
    Raycaster,
    Group,
    Color,
    AmbientLight,
    DirectionalLight,
    Scene,
    WebGLRenderer,
    PerspectiveCamera,
    Matrix4,
    Vector2,
    Object3D,
} from 'three'

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { SparseGrid, SparseGridData } from './SparseGrid'
import { PointCloud, PointData } from './PointCloud'
import { Spheres, SphereData } from './Spheres'
import { Cylinders, CylinderData } from './Cylinders'
import { Regions, RegionData } from './Regions'
import { LineSegments, LineSegmentData } from './LineSegments'
import { Grid } from './Grid'
import { TriangleMesh, TriangleMeshData } from './TriangleMesh'
import { Triangles, TriangleData } from './Triangles'
import * as TWEAKPANE from 'tweakpane'
import { Arrows, ArrowData } from './Arrows'

let pane = new TWEAKPANE.Pane()

function log(message: any) {
    let div = document.getElementById('console') as HTMLDivElement
    if (typeof message === 'object') div.textContent += JSON.stringify(message) + '\n'
    if (typeof message === 'string') div.textContent += message + '\n'
    div.scrollTop = div.scrollHeight
    console.log(message)
}

let mouse = new Vector2()

const renderer = new WebGLRenderer({ antialias: true, powerPreference: 'high-performance' })
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.setPixelRatio(window.devicePixelRatio)
document.body.appendChild(renderer.domElement)
const progressBar = document.getElementById('progress') as HTMLProgressElement
progressBar.style.visibility = 'hidden'

renderer.domElement.ondragover = function () {
    return false
}

renderer.domElement.ondragend = function () {
    return false
}

function loadXYZ(name: string, str: string) {
    var lines = str.split('\n')

    let pointData: PointData = {
        type: 'points',
        name: name,
        positions: new Array<Array<number>>(),
        colors: new Array<Array<number>>(),
    }
    let rescaleColors = false
    for (const line of lines) {
        const result = line.trim().split(/\s+/)
        const x = parseFloat(result[0])
        const y = parseFloat(result[1])
        const z = parseFloat(result[2])
        if (isNaN(x) || isNaN(y) || isNaN(z)) continue
        pointData.positions!.push([x, y, z])
        if (result.length >= 6) {
            const r = parseFloat(result[3])
            const g = parseFloat(result[4])
            const b = parseFloat(result[5])
            if (r > 1 || g > 1 || b > 1) rescaleColors = true
            pointData.colors!.push([r, g, b])
        }
    }
    if (rescaleColors)
        for (let color of pointData.colors!) {
            color[0] /= 255.0
            color[1] /= 255.0
            color[2] /= 255.0
        }

    return new PointCloud(pointData)
}

function loadPOBJ(name: string, str: string) {
    var lines = str.split('\n')

    let pointData: PointData = {
        type: 'points',
        name: name,
        positions: new Array<Array<number>>(),
        normals: new Array<Array<number>>(),
        colors: new Array<Array<number>>(),
    }

    for (const line of lines) {
        const result = line.trim().split(/\s+/)
        if (result[0] === 'v') {
            pointData.positions!.push([
                parseFloat(result[1]),
                parseFloat(result[2]),
                parseFloat(result[3]),
            ])
        }
        if (result[0] === 'vn') {
            pointData.normals!.push([
                parseFloat(result[1]),
                parseFloat(result[2]),
                parseFloat(result[3]),
            ])
        }
        if (result[0] === 'vc') {
            pointData.colors!.push([
                parseFloat(result[1]),
                parseFloat(result[2]),
                parseFloat(result[3]),
            ])
        }
    }
    if (pointData.normals && pointData.normals.length == 0) pointData.normals = undefined
    if (pointData.colors && pointData.colors.length == 0) pointData.colors = undefined
    return new PointCloud(pointData)
}

function loadBPC(name: string, arr: ArrayBuffer) {
    let pointData: PointData = {
        type: 'points',
        name: name,
        positions: new Array<Array<number>>(),
    }

    const array = [...new Float64Array(arr)]
    while (array.length) pointData.positions.push(array.splice(0, 3))

    return new PointCloud(pointData)
}

const scene = new Scene()

const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 3000)

function loadFile(file: File) {
    const extension = file.name.split('.').pop()?.toLocaleLowerCase()

    const reader = new FileReader()

    reader.onerror = () => {
        console.log(reader.error)
    }

    reader.onload = () => {
        if (extension == 'bpc') {
            const pc = loadBPC(file.name, reader.result as Float64Array)
            sceneRoot.add(pc)
        }
        if (extension == 'xyz') {
            const pc = loadXYZ(file.name, reader.result as string)
            sceneRoot.add(pc)
        } else if (extension == 'pobj') {
            const pc = loadPOBJ(file.name, reader.result as string)
            sceneRoot.add(pc)
        } else if (extension == 'json') {
            const data: Object = JSON.parse(reader.result as string)

            switch ((data as { type: string }).type) {
                case 'spheres':
                    sceneRoot.add(new Spheres(data as SphereData))
                    break
                case 'cylinders':
                    sceneRoot.add(new Cylinders(data as CylinderData))
                    break
                case 'points':
                    sceneRoot.add(new PointCloud(data as PointData))
                    break
                case 'sparse_grid':
                    sceneRoot.add(new SparseGrid(data as SparseGridData))
                    break
                case 'regions':
                    sceneRoot.add(new Regions(data as RegionData))
                    break
                case 'line_segments':
                    sceneRoot.add(new LineSegments(data as LineSegmentData))
                    break
                case 'triangle_mesh':
                    sceneRoot.add(new TriangleMesh(data as TriangleMeshData))
                    break
                case 'triangles':
                    sceneRoot.add(new Triangles(data as TriangleData))
                    break
                case 'arrows':
                    sceneRoot.add(new Arrows(data as ArrowData))
                    break
            }
        }
    }

    if (['xyz', 'pobj', 'json'].indexOf(extension!) >= 0) reader.readAsText(file)
    else if (['bpc'].indexOf(extension!) >= 0) reader.readAsArrayBuffer(file)
    else alert('Cannot read dropped file')
}

renderer.domElement.ondrop = function (ev: DragEvent) {
    ev.preventDefault()

    for (const file of ev.dataTransfer?.files!) loadFile(file)
}

const controls = new OrbitControls(camera, renderer.domElement)

function resetCamera() {
    controls.reset()
    camera.position.x = 10
    camera.position.y = 10
    camera.position.x = 10
    camera.lookAt(0, 0, 0)
    camera.updateProjectionMatrix()
    controls.update()
}

resetCamera()

scene.background = new Color(0xc0c0c0)

const grid = new Grid(camera, 1, 10, 400)
scene.add(grid)

let settingsFolder = pane.addFolder({ title: 'View Settings' })
settingsFolder.addInput(grid, 'visible', {
    label: 'show grid',
})
let experimentalFolder = pane.addFolder({ title: 'Experimental', expanded: false })

let PARAMS = {
    name: 'unnamed',
}

let input = experimentalFolder.addInput(PARAMS, 'name')

const btn1 = experimentalFolder.addButton({
    title: 'getName',
    label: 'send',
})

btn1.on('click', () => {
    fetch('https://localhost:8081/name/')
        .then((response) => response.json())
        .then((data) => {
            log(data)
            PARAMS.name = data.name
            input.refresh()
        })
        .catch((e) => {
            log(`Caught error: ${e.message}`)
        })
})

const btn2 = experimentalFolder.addButton({
    title: 'setName',
    label: 'send',
})

btn2.on('click', () => {
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(PARAMS),
    }
    fetch('https://localhost:8081/name/', requestOptions)
        .then((response) => response.json())
        .then((data) => log(data))
        .catch((e) => {
            log(`Caught error: ${e.message}`)
        })
})

/*

btn.on('click', () => {
    fetch('https://localhost:8081/name/').then((response) => console.log(response))
})*/

const ambientLight = new AmbientLight(0x505050)
scene.add(ambientLight)

const directionalLight = new DirectionalLight(0xffffff)
directionalLight.position.set(1, 0.75, 0.5).normalize()
scene.add(directionalLight)

var sceneRoot = new Group()
scene.add(sceneRoot)
/*sceneRoot.setRotationFromMatrix(
    new Matrix4().set(
        1.0,
        0.0,
        0.0,
        0.0,

        0.0,
        0.0,
        1.0,
        0.0,

        0.0,
        -1.0,
        0.0,
        0.0,

        0.0,
        0.0,
        0.0,
        1.0
    )
)*/

window.addEventListener('resize', onWindowResize, false)

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

function animate() {
    requestAnimationFrame(animate)
    controls.update()
    render()
}

function render() {
    renderer.render(scene, camera)
}

document.onkeydown = (e: KeyboardEvent) => {
    switch (e.key) {
        case 'g':
            grid.visible = !grid.visible
            break
        case ' ':
            resetCamera()
            break
    }
}

function onMouseMove(event: MouseEvent) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components

    mouse.x = (event.clientX / window.innerWidth) * 2 - 1
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
}

let raycaster = new Raycaster()
let selectedObject: Object3D | undefined = undefined

function onClick(event: MouseEvent) {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1
    /* raycaster.setFromCamera(mouse, camera)
    let intersects = raycaster.intersectObject(sceneRoot, true)
    log(mouse)

    if (intersects.length > 0) {
        selectedObject = intersects[0].object
        log(selectedObject.name)
        return
    } else {
        selectedObject = undefined
    }*/
}

window.addEventListener('click', onClick)
window.addEventListener('mousemove', onMouseMove, false)

animate()
