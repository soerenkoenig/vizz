import {
    Object3D,
    Mesh,
    Float32BufferAttribute,
    BufferGeometry,
    BufferAttribute,
    DoubleSide,
    EdgesGeometry,
    LineSegments,
    LineBasicMaterial,
    MeshPhongMaterial,
} from 'three'

export type TriangleMeshData = {
    type: 'triangle_mesh'
    color?: number
    wire_frame?: boolean
    vertices: Array<Array<number>>
    triangles: Array<Array<number>>
}

export class TriangleMesh extends Object3D {
    constructor(data: TriangleMeshData) {
        super()

        let color = 0x12345
        if (data.color) color = data.color

        const wireFrame = data.wire_frame ?? true

        let geometry = new BufferGeometry()

        geometry.setAttribute('position', new Float32BufferAttribute(data.vertices.flat(), 3))

        let indices = new Uint32Array(data.triangles.flat())
        geometry.setIndex(new BufferAttribute(indices, 1))
        geometry.computeVertexNormals()
        if (wireFrame) {
            const materialLines = new LineBasicMaterial({ color: color })
            let edgeGeometry = new EdgesGeometry(geometry, 0)
            let lines = new LineSegments(edgeGeometry, materialLines)
            this.add(lines)
        } else {
            const material = new MeshPhongMaterial({ color: color, side: DoubleSide })
            const mesh = new Mesh(geometry, material)
            this.add(mesh)
        }
    }
}
