import {
    DoubleSide,
    Object3D,
    Mesh,
    ArrowHelper,
    CylinderGeometry,
    MeshLambertMaterial,
    Vector3,
    ConeGeometry,
} from 'three'

export interface ArrowData {
    type: 'arrows'
    name?: string
    color?: number
    primitives: Array<Array<number>>
}

export class Arrows extends Object3D {
    constructor(data: ArrowData) {
        super()

        let color = 0xffffff
        if (data.color) color = data.color

        if (data.name) this.name = data.name

        const material = new MeshLambertMaterial({
            color: color,
            side: DoubleSide,
        })

        for (let arrow of data.primitives) {
            const start = new Vector3(arrow[0], arrow[1], arrow[2])
            const end = new Vector3(arrow[3], arrow[4], arrow[5])

            var direction = new Vector3().subVectors(end, start)
            const radius = 0.02 * 0.5 * direction.length()
            const arr = new ArrowHelper(direction, start, undefined, color)

            var edgeGeometry = new CylinderGeometry(radius, radius, direction.length() * 0.8, 8, 4)
            var edge = new Mesh(edgeGeometry, material)
            edge.setRotationFromEuler(arr.rotation)
            edge.position.copy(
                new Vector3().addVectors(start, direction.clone().multiplyScalar(0.8 * 0.5))
            )

            let coneGeometry = new ConeGeometry(
                0.04 * direction.length(),
                0.2 * direction.length(),
                undefined,
                8,
                false
            )
            var cone = new Mesh(coneGeometry, material)
            cone.setRotationFromEuler(arr.rotation)
            cone.position.copy(
                new Vector3().addVectors(start, direction.clone().multiplyScalar(0.9))
            )

            this.add(cone)
            this.add(edge)
        }
    }
}
