import { Color, Object3D, AxesHelper, PerspectiveCamera } from 'three'
import { InfiniteGrid } from './InfiniteGrid'
import { Arrows } from './Arrows'

export class Grid extends Object3D {
    constructor(camera: PerspectiveCamera, size1?: number, size2?: number, distance?: number) {
        super()

        const arrowX = new Arrows({
            type: 'arrows',
            color: 0xff0000,
            primitives: [[0.0, 0.0, 0.0, 1.0, 0.0, 0.0]],
        })
        const arrowY = new Arrows({
            type: 'arrows',
            color: 0x00ff00,
            primitives: [[0.0, 0.0, 0.0, 0.0, 1.0, 0.0]],
        })
        const arrowZ = new Arrows({
            type: 'arrows',
            color: 0x0000ff,
            primitives: [[0.0, 0.0, 0.0, 0.0, 0.0, 1.0]],
        })

        this.add(arrowX)
        this.add(arrowY)
        this.add(arrowZ)

        this.grid = new InfiniteGrid(size1, size2, new Color(0x505050), distance)
        this.add(this.grid)
    }

    private grid: InfiniteGrid
}
