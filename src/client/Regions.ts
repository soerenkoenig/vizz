import {
    Object3D,
    Vector2,
    Mesh,
    Shape,
    ShapeGeometry,
    Path,
    DoubleSide,
    MeshLambertMaterial,
} from 'three'

type Region = {
    color?: number
    Outerloop: Array<Array<number>>
    Innerloops?: Array<Array<Array<number>>>
    matLcsToWcs?: Array<number>
}

export type RegionData = {
    type: 'regions'
    name?: string
    color?: number
    primitives: Region[]
}

function convertArray(points: Array<Array<number>>) {
    let vectors: Array<Vector2> = []

    for (const point of points) vectors.push(new Vector2(point[0], point[1]))
    return vectors
}

export class Regions extends Object3D {
    constructor(data: RegionData) {
        super()

        if (data.name) this.name = data.name

        let color = 0x101010
        if (data.color) color = data.color
        const material = new MeshLambertMaterial({
            color,
            side: DoubleSide,
        })

        for (let region of data.primitives) {
            const boundaryShape = new Shape(convertArray(region.Outerloop))

            if (region.Innerloops) {
                for (const hole of region.Innerloops) {
                    const holePath = new Path(convertArray(hole))

                    boundaryShape.holes.push(holePath)
                }
            }
            let currentMaterial = undefined
            if (region.color) {
                currentMaterial = new MeshLambertMaterial({
                    color: region.color,
                    side: DoubleSide,
                })
            } else {
                currentMaterial = material
            }

            let mesh = new Mesh(new ShapeGeometry(boundaryShape), currentMaterial)

            if (region.matLcsToWcs) {
                mesh.matrixAutoUpdate = false

                mesh.matrix.set(
                    region.matLcsToWcs[0],
                    region.matLcsToWcs[1],
                    region.matLcsToWcs[2],
                    region.matLcsToWcs[3],
                    region.matLcsToWcs[4],
                    region.matLcsToWcs[5],
                    region.matLcsToWcs[6],
                    region.matLcsToWcs[7],
                    region.matLcsToWcs[8],
                    region.matLcsToWcs[9],
                    region.matLcsToWcs[10],
                    region.matLcsToWcs[11],
                    region.matLcsToWcs[12],
                    region.matLcsToWcs[13],
                    region.matLcsToWcs[14],
                    region.matLcsToWcs[15]
                )

                mesh.matrixWorldNeedsUpdate = true
            }

            this.add(mesh)
        }
    }
}
