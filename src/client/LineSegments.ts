import * as THREE from 'three'

export interface LineSegmentData {
    type: 'line_segments'
    name?: string
    color?: number
    primitives: Array<Array<number>>
}

export class LineSegments extends THREE.LineSegments {
    constructor(data: LineSegmentData) {
        super()
        if (data.name) this.name = data.name

        let color = 0x101010
        if (data.color) color = data.color

        let geom = new THREE.BufferGeometry()
        geom.setAttribute('position', new THREE.Float32BufferAttribute(data.primitives.flat(), 3))
        geom.computeBoundingSphere()

        var material = new THREE.LineBasicMaterial({ color: color })
        this.geometry = geom
        this.material = material
    }
}
