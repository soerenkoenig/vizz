import {
    BufferGeometry,
    Points,
    Float32BufferAttribute,
    ShaderMaterial,
    Uniform,
    Color,
} from 'three'

export interface PointData {
    type: 'points'
    name?: string
    color?: number
    pointSize?: number
    positions: Array<Array<number>>
    normals?: Array<Array<number>>
    colors?: Array<Array<number>>
}

export class PointCloud extends Points {
    constructor(data: PointData) {
        super()
        let pointSize = 25
        if (data.pointSize) pointSize = data.pointSize

        let color = 0xffffff
        if (data.color) color = data.color

        let geom = new BufferGeometry()
        geom.setAttribute('position', new Float32BufferAttribute(data.positions.flat(), 3))
        if (data.normals)
            geom.setAttribute('normal', new Float32BufferAttribute(data.normals.flat(), 3))
        if (data.colors)
            geom.setAttribute('color', new Float32BufferAttribute(data.colors.flat(), 3))
        geom.computeBoundingSphere()

        const uniforms = {
            pointSize: { value: pointSize },
            uniformColor: new Uniform(new Color(color)),
        }

        let perPointColor = false
        if (data.colors) perPointColor = true
        let hasNormals = false
        if (data.normals) hasNormals = true

        // point cloud material
        const shaderMaterial = new ShaderMaterial({
            uniforms: uniforms,
            vertexShader: PointCloud.vertexShader,
            fragmentShader: PointCloud.fragmentShader,
            transparent: true,
            defines: { PER_POINT_COLOR: perPointColor, USE_NORMALS: hasNormals },
        })
        this.geometry = geom
        this.material = shaderMaterial

        if (data.name) this.name = data.name
    }

    set pointSize(value: number) {
        ;(this.material as ShaderMaterial).uniforms['pointSize'].value = value
    }

    get pointSize(): number {
        return (this.material as ShaderMaterial).uniforms['pointSize'].value
    }

    set uniformColor(value: Color) {
        ;(this.material as ShaderMaterial).uniforms['uniformColor'].value = value
    }

    get uniformColor(): Color {
        return (this.material as ShaderMaterial).uniforms['uniformColor'].value
    }

    private static vertexShader: string = `
    uniform float pointSize;
    uniform vec3 uniformColor;
    
    #if defined(PER_POINT_COLOR)
    attribute vec3 color;
    #endif
    
    varying vec3 vColor;

    #if defined(USE_NORMALS)
    varying vec3 vNormal;
    #endif
    varying vec4 mvPosition;

    

    void main() {
     
      #if defined(PER_POINT_COLOR)
      vColor = color;
      #else
      vColor = uniformColor;
      #endif

      #if defined(USE_NORMALS)
      vNormal = normalMatrix * normal;
      #endif

      mvPosition = modelViewMatrix * vec4( position, 1.0 ); 
      
      gl_PointSize = - ( pointSize / mvPosition.z );
      gl_Position = projectionMatrix * mvPosition;    
    }
  `

    private static fragmentShader: string = `
    uniform float pointSize;
    varying vec3 vColor;
    varying vec4 mvPosition;
    #if defined(USE_NORMALS)
    varying vec3 vNormal;
    #endif

    void main() {
       
      // distance = len(x: [-1, 1], y: [-1, 1])
      float distance = length(2.0 * gl_PointCoord - 1.0);
      if (distance > 1.0) {
        discard;
      }

      #if defined(USE_NORMALS)
      
       vec3 norm = normalize(vNormal);
       vec3 lightDir = normalize( - mvPosition.xyz);
       float diff = min(abs(dot(norm, lightDir)), 1.0);
       gl_FragColor = vec4(diff*vColor.rgb, 1.0);  
       #else
       
       gl_FragColor = vec4(vColor.rgb, 1.0);
      #endif

     
      
      // we add a small depth offset to draw points above other geometry
      gl_FragDepth = gl_FragCoord.z - 0.001;  
    }
  `
}
