import { BoxGeometry, Object3D, EdgesGeometry, LineSegments, LineBasicMaterial } from 'three'

export type SparseGridData = {
    type: 'sparse_grid'
    color?: number
    cell_size: number
    cell_centered?: boolean
    cell_indices: Array<Array<number>>
}
export class SparseGrid extends Object3D {
    constructor(data: SparseGridData) {
        super()
        let boxGeometry = new BoxGeometry(data.cell_size, data.cell_size, data.cell_size)
        let edgeGeometry = new EdgesGeometry(boxGeometry)
        let color = 0x202020
        if (data.color) color = data.color

        const material = new LineBasicMaterial({ color: color })
        const offset = data.cell_centered !== undefined && data.cell_centered! ? 0 : 0.5

        for (let cellIndex of data.cell_indices) {
            let line = new LineSegments(edgeGeometry, material)
            line.position
                .set(cellIndex[0], cellIndex[1], cellIndex[2])
                .addScalar(offset)
                .multiplyScalar(data.cell_size)
            this.add(line)
        }
    }
}
