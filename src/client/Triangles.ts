import {
    Object3D,
    Mesh,
    Float32BufferAttribute,
    BufferGeometry,
    MeshLambertMaterial,
    DoubleSide,
    EdgesGeometry,
    LineSegments,
    LineBasicMaterial,
    Vector3,
} from 'three'

export type TriangleData = {
    type: 'triangles'
    color?: number
    wire_frame?: boolean
    primitives: Array<Array<number>>
    face_normals?: Array<Array<number>>
    vertex_normals?: Array<Array<number>>
}

export class Triangles extends Object3D {
    constructor(data: TriangleData) {
        super()

        let color = 0x12345
        if (data.color) color = data.color

        let wireFrame = false
        if (data.wire_frame) wireFrame = data.wire_frame

        let geometry = new BufferGeometry()

        geometry.setAttribute('position', new Float32BufferAttribute(data.primitives.flat(), 3))
        const pA = new Vector3()
        const pB = new Vector3()
        const pC = new Vector3()

        const cb = new Vector3()
        const ab = new Vector3()
        let normals = new Array<number>()

        if (data.vertex_normals) normals = data.vertex_normals.flat()
        else if (data.face_normals) {
            for (const faceNormal of data.face_normals) {
                const nx = faceNormal[0]
                const ny = faceNormal[1]
                const nz = faceNormal[2]

                normals.push(nx, ny, nz)
                normals.push(nx, ny, nz)
                normals.push(nx, ny, nz)
            }
        } else {
            for (const triangle of data.primitives) {
                pA.set(triangle[0], triangle[1], triangle[2])
                pB.set(triangle[3], triangle[4], triangle[5])
                pC.set(triangle[6], triangle[7], triangle[8])
                cb.subVectors(pC, pB)
                ab.subVectors(pA, pB)
                cb.cross(ab)
                cb.normalize()
                const nx = cb.x
                const ny = cb.y
                const nz = cb.z

                normals.push(nx, ny, nz)
                normals.push(nx, ny, nz)
                normals.push(nx, ny, nz)
            }
        }
        geometry.setAttribute('normal', new Float32BufferAttribute(normals, 3))
        geometry.computeBoundingSphere()

        if (wireFrame) {
            const materialLines = new LineBasicMaterial({ color: color })
            let edgeGeometry = new EdgesGeometry(geometry, 0)
            let lines = new LineSegments(edgeGeometry, materialLines)
            this.add(lines)
        } else {
            const material = new MeshLambertMaterial({ color: color, side: DoubleSide })
            const mesh = new Mesh(geometry, material)
            this.add(mesh)
        }
    }
}
