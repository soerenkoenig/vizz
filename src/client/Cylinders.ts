import { DoubleSide, Object3D, Mesh, CylinderGeometry, MeshLambertMaterial, Vector3 } from 'three'

export interface CylinderData {
    type: 'cylinders'
    name?: string
    color?: number
    primitives: Array<Array<number>>
}

export class Cylinders extends Object3D {
    constructor(data: CylinderData) {
        super()

        const geometries: CylinderGeometry[] = []
        const unitApproximationErrors: number[] = []
        const approximationErrorThreshold = 0.01

        for (let radialSegments = 5; radialSegments < 50; radialSegments += 5) {
            geometries.push(new CylinderGeometry(1.0, 1.0, 1.0, radialSegments, 1, true))
            unitApproximationErrors.push(
                Math.PI - radialSegments * Math.sin(Math.PI / radialSegments)
            )
        }

        let color = 0xffffff
        if (data.color) color = data.color

        if (data.name) this.name = data.name

        const material = new MeshLambertMaterial({
            color: color,
            side: DoubleSide,
        })

        for (let cylinder of data.primitives) {
            const radius = cylinder[6]

            // choose level of detail via radius
            let detail = 0
            for (const err of unitApproximationErrors) {
                if (err * radius <= approximationErrorThreshold) break
                if (detail < unitApproximationErrors.length - 1) detail++
            }

            let mesh = new Mesh(geometries[detail], material)
            const start = new Vector3(cylinder[0], cylinder[1], cylinder[2])
            const end = new Vector3(cylinder[3], cylinder[4], cylinder[5])
            let delta = end.clone().sub(start)
            const height = delta.length()
            mesh.position.copy(start.clone().add(delta.clone().multiplyScalar(0.5)))
            mesh.scale.set(radius, height, radius)
            delta = delta.normalize()

            const yAxis = new Vector3(0, 1, 0)
            const dir = yAxis.clone().cross(delta).normalize()
            mesh.setRotationFromAxisAngle(dir, yAxis.angleTo(delta))
            this.add(mesh)
        }
    }
}
