import { Object3D, Mesh, IcosahedronGeometry, MeshLambertMaterial } from 'three'

export type SphereData = {
    type: 'spheres'
    color?: number
    primitives: Array<Array<number>>
}

export class Spheres extends Object3D {
    constructor(data: SphereData) {
        super()

        const geometries: IcosahedronGeometry[] = []
        for (let detail = 0; detail < 5; ++detail)
            geometries.push(new IcosahedronGeometry(1.0, detail))

        let color = 0xffffff
        if (data.color) color = data.color

        const material = new MeshLambertMaterial({ color: color })

        for (let sphere of data.primitives) {
            //todo select detail level by radius
            const detail = 3
            let mesh = new Mesh(geometries[detail], material)
            mesh.position.set(sphere[0], sphere[1], sphere[2])
            mesh.scale.set(sphere[3], sphere[3], sphere[3])
            this.add(mesh)
        }
    }
}
